﻿#pragma strict
import UnityEngine.UI; // Needed for ui text to work

static var pickedUp = 0;

function Start () {

	pickedUp = 0;
}

var needed = 7; 
var scoreText : Text;


function Update () {
		
	Debug.Log("TOTAL PICKED UP = " + pickedUp);
		
	scoreText.text = "Clues : " + pickedUp.ToString() + " / " + needed.ToString();
		
	// Check if player collect the needed amount
	if(pickedUp == needed){
	
	
        
        Debug.Log("Case Solved");
        Application.LoadLevel(1);
    
    }
}