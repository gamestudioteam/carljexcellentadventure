﻿#pragma strict
import UnityEngine.UI; // Needed for ui text to work

static var pickedUp = 0;

var needed = 14; 
var scoreText : Text;
var player : GameObject;

function Start(){
	pickedUp = 0;
}

function Update () {
		
	Debug.Log("TOTAL PICKED UP = " + pickedUp + " / " + needed);
		
	scoreText.text = pickedUp.ToString() + " / " + needed.ToString();
		
	// Check if player collect the needed amount
	if(pickedUp == needed){
	
		//this.gameObject.renderer.enabled = false; // turn off graphics
		//this.gameObject.collider.enabled = false; // turn off collider
		
		Debug.Log("GAME COMPLETE");
		scoreText.text = pickedUp.ToString() + " / " + needed.ToString() + " Congratulations!";
		Application.LoadLevel(0);
	
	}
}